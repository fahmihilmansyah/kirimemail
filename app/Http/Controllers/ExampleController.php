<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;


class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    function Kirim(Request $req){
        try {
            $json = $req->all();
            $data['html'] = $json['html'];
//            print_r($json);exit;
            Mail::send([],$data, function ($message) use ($json) {
                $message->to($json['to']);
                $message->from('noreply@ganeshabisa.com',$json['fromName']);
                $message->subject($json['subject']);
                $message->setBody($json['html'],'text/html');
            });
//            Mail::raw($json['html'], function ($message) use ($json) {
//                $message->to($json['to']);
//                $message->from('noreply@ganeshabisa.com',$json['fromName']);
//                $message->subject($json['subject']);
//            });
        }catch (\Exception $e){
            print_r($e->getMessage());
        }
    }
    function Kirim2(Request $req){
        $response=[];
        try {
            /*
           $conf is an array containing the mail configuration,
           a described in config/mail.php. Something like:

           [
               'driver' => 'smtp',
               'host' => 'smtp.mydomain.com',
               'username' => foo',
               'password' => 'bar'
               ...
           ]
       */
/*
 * {"config":{"type":"smtp","host":"smtp.hostinger.co.id","username":"noreply@mumuapps.id","password":"26Juni1992","port":"587","encryption":"tls"},"to":"fahmi.hilmansyah@gmail.com","fromName":"sayafromname","subject":"ini subject","html":"test html isi"}
 * */
            $app = App::getInstance();
            $app->register('Illuminate\Mail\MailServiceProvider');

            $json = $req->all();
            if(!empty($json['config'])){
                $jconf = $json['config'];
                $usernamemail = $jconf['username'];
                $conf = ['driver' => empty($jconf['type'])?'smtp':$jconf['type'],
                    'host' => $jconf['host'],
                    'username' => $jconf['username'],
                    'password' => $jconf['password'],
                    'port'=>$jconf['port'],
                    'encryption'=>$jconf['encryption']
                ];
//                print_r($conf);exit;
//                $conf = ['driver' => 'smtp',
//                    'host' => 'smtp.gmail.com',
//                    'username' => 'sempak.melar1010@gmail.com',
//                    'password' => '26Juni1992',
//                    'port'=>587,
//                    'encryption'=>'tls'
//                ];
                Config::set('mail', $conf);
            }
            $data['html'] = $json['html'];
//            print_r($json);exit;
            $res = Mail::send([],$data, function ($message) use ($json) {
                $message->to($json['to']);
                $message->from($json['config']['username'],$json['fromName']);
                $message->subject($json['subject']);
                $message->setBody($json['html'],'text/html');
            });

//            Mail::raw($json['html'], function ($message) use ($json) {
//                $message->to($json['to']);
//                $message->from('noreply@ganeshabisa.com',$json['fromName']);
//                $message->subject($json['subject']);
//            });
            $response['rc']=200;
            $response['status']='success';
            $response['message']='Ok';
        }catch (\Exception $e){
//            print_r($e->getMessage());
            $response['rc']=114;
            $response['status']='failed';
            $response['message']=$e->getMessage();
        }
        return $response;
    }
}
